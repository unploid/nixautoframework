package nix.training.autoframework.spec

import geb.spock.GebReportingSpec
import nix.training.autoframework.page.BlogPage
import nix.training.autoframework.page.StartPage

class NixNavigationSpec extends GebReportingSpec{
    def "Navigate to Blog page"(){
        when:
            to StartPage
        and:
            "User navigates to Blog page"()
        then:
            at BlogPage


    }
}
